function evaluateExpression() {
    try {
        var expression = document.getElementById('expression').value;
        document.getElementById('expression').value = eval(expression);
    } catch (e) {
        document.getElementById('expression').value = 'ERR';
    }

    return false;
}


function deleteLastChar() {
    var newB = window.document.forms['form-expression'].elements.expression.value;
    document.getElementById('expression').value = newB.substr(0, newB.length - 1);
    // console.log(newB);

    // return newB;
}

function newNumer(number) {
    var lastValue = window.document.forms['form-expression'].elements.expression.value;
    window.document.forms['form-expression'].elements.expression.value = lastValue + number;
}

function newOperator(operator) {
    var lastValue = window.document.forms['form-expression'].elements.expression.value;
    console.log('lastValue', lastValue, 'operator', operator);
    window.document.forms['form-expression'].elements.expression.value = lastValue + operator;
}