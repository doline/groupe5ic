function addNumer() {
    var tabNumber = [7,8,9];
    var tabHtmlElts = [];
    for(var i=0; i<tabNumber.length; i++) {
        tabHtmlElts.push('<button onclick="newNumer('+tabNumber[i]+')">'+tabNumber[i]+'</button>');
    }
    document.getElementById('number-area').innerHTML += tabHtmlElts.join(' ');
}

function addOperators() {
    var tabNumber = ['+', '-', '*', '/', '(', ')'];
    var tabHtmlElts = [];
    for(var i=0; i<tabNumber.length; i++) {
        tabHtmlElts.push('<button onclick="newOperator(\''+tabNumber[i]+'\')">'+tabNumber[i]+'</button>');
    }
    document.getElementById('operator-area').innerHTML += tabHtmlElts.join(' ');
}

function main() {
    addNumer();
    addOperators();
}